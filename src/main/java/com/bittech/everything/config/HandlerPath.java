package com.bittech.everything.config;

import lombok.Data;

import java.util.Set;

/**
 * Author: secondriver
 * Created: 2018/10/10
 */
@Data
public class HandlerPath {
    
    /**
     * 包含的路径
     */
    private final Set<String> includes;
    
    /**
     * 排除的路径
     */
    private final Set<String> excludes;
}
