package com.bittech.everything.core.dao;


import com.bittech.everything.core.model.Condition;
import com.bittech.everything.core.model.Thing;

import java.util.List;

/**
 * Author: secondriver
 * Created: 2018/9/26
 */
public interface FileIndexDao {
    
    /**
     * 插入
     *
     * @param thing
     */
    void insert(Thing thing);
    
    /**
     * 删除
     *
     * @param thing
     */
    void delete(Thing thing);
    
    /**
     * 根据条件查询
     *
     * @param condition
     * @return
     */
    List<Thing> query(Condition condition);
}
