package com.bittech.everything.core.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Author: secondriver
 * Created: 2018/9/26
 */
public enum FileType {
    /**
     * 图片类型
     */
    IMG("png", "jpeg", "jpg", "gif"),
    /**
     * 文档类型
     */
    DOC("doc", "docx", "xls", "xlsx", "ppt", "pptx", "xls", "xlsx", "pdf", "txt", "md"),
    /**
     * 归档类型
     */
    ARCHIVE("zip", "rar", "7z"),
    /**
     * 可执行类型
     */
    BIN("exe", "msi", "sh", "jar"),
    /**
     * 其它类型
     */
    OTHER("*");
    
    private Set<String> extend = new HashSet<>();
    
    FileType(String... extend) {
        this.extend.addAll(Arrays.asList(extend));
    }
    
    /**
     * 根据扩展名称查询文件类型
     *
     * @param extend
     * @return
     */
    public static FileType lookup(String extend) {
        for (FileType fileType : values()) {
            if (fileType.extend.contains(extend)) {
                return fileType;
            }
        }
        return OTHER;
    }
    
    /**
     * 根据类型名称查询文件类型
     *
     * @param name
     * @return
     */
    public static FileType lookupByName(String name) {
        for (FileType fileType : values()) {
            if (fileType.name().equalsIgnoreCase(name)) {
                return fileType;
            }
        }
        return OTHER;
    }
}