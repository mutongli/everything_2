package com.bittech.everything.core.model;

import lombok.Data;

/**
 * 检索条件模型类
 * <p>
 * Author: secondriver
 * Created: 2018/9/26
 */
@Data
public class Condition {
    
    /**
     * 文件名称
     */
    private String name;
    
    /**
     * 文件类型（大分类）
     */
    private String fileType;
    
    /**
     * 限制数量
     */
    private Integer limit;
    
    /**
     * 深度排序策略
     */
    private boolean orderByAsc;
}
