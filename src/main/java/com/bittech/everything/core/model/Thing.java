package com.bittech.everything.core.model;

import lombok.Data;

/**
 * Author: secondriver
 * Created: 2018/9/26
 */
@Data
public class Thing {
    
    /**
     * 文件名
     */
    private String name;
    
    /**
     * 文件类型（大分类）
     */
    private FileType fileType;
    
    /**
     * 文件路径
     */
    private String path;
    
    /**
     * 文件路径深度
     */
    private int depth;
    
}