package com.bittech.everything.core.search;

import com.bittech.everything.core.model.Condition;
import com.bittech.everything.core.model.Thing;

import java.util.List;

/**
 * Author: secondriver
 * Created: 2018/9/26
 */
public interface FileSearch {
    
    /**
     * 根据条件检索文件信息
     *
     * @param condition
     * @return
     */
    List<Thing> search(Condition condition);
}