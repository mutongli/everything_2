package com.bittech.everything.core.search.impl;

import com.bittech.everything.core.model.Condition;
import com.bittech.everything.core.model.Thing;
import com.bittech.everything.core.dao.FileIndexDao;
import com.bittech.everything.core.search.FileSearch;

import java.util.List;

/**
 * Author: secondriver
 * Created: 2018/9/27
 */
public class FileSearchImpl implements FileSearch {
    
    private final FileIndexDao fileIndexDao;
    
    public FileSearchImpl(FileIndexDao fileIndexDao) {
        this.fileIndexDao = fileIndexDao;
     
    }
    
    @Override
    public List<Thing> search(Condition condition) {
        return this.fileIndexDao.query(condition);
    }
}