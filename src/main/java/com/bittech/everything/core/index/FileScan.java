package com.bittech.everything.core.index;

import com.bittech.everything.core.interceptor.FileInterceptor;

/**
 * Author: secondriver
 * Created: 2018/9/26
 */
public interface FileScan {
    
    /**
     * 索引指定path
     *
     * @param path
     */
    void index(String path);
    
    /**
     * 添加拦截器
     *
     * @param fileInterceptor
     */
    void interceptor(FileInterceptor fileInterceptor);
    
}
