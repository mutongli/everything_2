package com.bittech.everything.core.interceptor.impl;

import com.bittech.everything.core.interceptor.FileInterceptor;

import java.io.File;

/**
 * Author: secondriver
 * Created: 2018/9/26
 */
public class FilePrintInterceptor implements FileInterceptor {
    
    @Override
    public void applyFile(File file) {
        System.out.println(String.format("%s:%s", file.isDirectory() ? "d" : "f", file.getName()));
    }
}
