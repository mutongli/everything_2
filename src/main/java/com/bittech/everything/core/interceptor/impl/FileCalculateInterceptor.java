package com.bittech.everything.core.interceptor.impl;

import com.bittech.everything.core.interceptor.FileInterceptor;

import java.io.File;

/**
 * Author: secondriver
 * Created: 2019/1/23
 * Description: 比特科技，只为更好的你；你只管学习，其它交给我。
 */
public class FileCalculateInterceptor implements FileInterceptor {
    
    private volatile int count = 0;
    
    @Override
    public synchronized void applyFile(File file) {
        count++;
    }
    
    public int getCount() {
        return count;
    }
}
