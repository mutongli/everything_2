package com.bittech.everything.core.interceptor.impl;

import com.bittech.everything.core.dao.FileIndexDao;
import com.bittech.everything.core.interceptor.ThingInterceptor;
import com.bittech.everything.core.model.Thing;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Author: secondriver
 * Created: 2019/2/15
 * Description: 比特科技，只为更好的你；你只管学习，其它交给我。
 */
public class ThingClearInterceptor implements ThingInterceptor, Runnable {
    
    private final Queue<Thing> queue = new LinkedBlockingDeque<>(1024);
    
    private final FileIndexDao fileIndexDao;
    
    public ThingClearInterceptor(FileIndexDao fileIndexDao) {
        this.fileIndexDao = fileIndexDao;
    }
    
    @Override
    public void applyThing(Thing thing) {
        this.queue.add(thing);
    }
    
    @Override
    public void run() {
        while (true) {
            Thing thing = queue.poll();
            if (thing != null) {
                fileIndexDao.delete(thing);
            }
        }
    }
}
