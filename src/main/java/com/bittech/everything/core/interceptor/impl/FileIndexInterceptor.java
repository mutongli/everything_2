package com.bittech.everything.core.interceptor.impl;

import com.bittech.everything.core.common.FileConvertThing;
import com.bittech.everything.core.interceptor.FileInterceptor;
import com.bittech.everything.core.model.Thing;
import com.bittech.everything.core.dao.FileIndexDao;

import java.io.File;

/**
 * Author: secondriver
 * Created: 2018/9/26
 */
public class FileIndexInterceptor implements FileInterceptor {
    
    private final FileIndexDao fileIndexDao;
    
    public FileIndexInterceptor(FileIndexDao fileIndexDao) {
        this.fileIndexDao = fileIndexDao;
    }
    
    @Override
    public void applyFile(File file) {
        Thing thing = FileConvertThing.convert(file);
        fileIndexDao.insert(thing);
    }
}
