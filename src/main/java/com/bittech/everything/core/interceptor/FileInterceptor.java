package com.bittech.everything.core.interceptor;

import java.io.File;

/**
 * 文件拦截器
 * <p>
 * Author: secondriver
 * Created: 2018/9/26
 */
@FunctionalInterface
public interface FileInterceptor {
    
    /**
     * 应用文件
     *
     * @param file
     */
    void applyFile(File file);
    
}
