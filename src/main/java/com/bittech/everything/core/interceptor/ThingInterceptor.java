package com.bittech.everything.core.interceptor;

import com.bittech.everything.core.model.Thing;

/**
 * Thing拦截器
 * <p>
 * Author: secondriver
 * Created: 2018/9/27
 */
@FunctionalInterface
public interface ThingInterceptor {
    
    void applyThing(Thing thing);
}
