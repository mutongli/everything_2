package com.bittech.everything.core.monitor;

import com.bittech.everything.config.HandlerPath;

/**
 * Author: secondriver
 * Created: 2018/9/29
 */
public interface FileWatcher {
    
    /**
     * 启动文件监控
     */
    void start();
    
    /**
     * 设置监控处理的path
     *
     * @param handlerPath
     */
    void monitor(HandlerPath handlerPath);
    
    /**
     * 停止文件监控
     */
    void stop();
}
