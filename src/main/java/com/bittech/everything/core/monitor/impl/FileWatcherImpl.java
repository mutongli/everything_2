package com.bittech.everything.core.monitor.impl;

import com.bittech.everything.config.HandlerPath;
import com.bittech.everything.core.common.FileConvertThing;
import com.bittech.everything.core.model.Thing;
import com.bittech.everything.core.dao.FileIndexDao;
import com.bittech.everything.core.index.FileScan;
import com.bittech.everything.core.monitor.FileWatcher;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

import java.io.File;

/**
 * Author: secondriver
 * Created: 2018/10/10
 */
public class FileWatcherImpl extends FileAlterationListenerAdaptor implements FileWatcher {
    
    private final FileAlterationMonitor monitor;
    
    private final FileScan fileScan;
    
    private final FileIndexDao fileIndexDao;
    
    public FileWatcherImpl(FileScan fileScan, FileIndexDao fileIndexDao, long interval) {
        this.fileScan = fileScan;
        this.fileIndexDao = fileIndexDao;
        this.monitor = new FileAlterationMonitor(interval);
    }
    
    @Override
    public void stop() {
        try {
            monitor.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public void start() {
        try {
            monitor.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    @Override
    public void monitor(HandlerPath handlerPath) {
        for (String path : handlerPath.getIncludes()) {
            FileAlterationObserver observer = new FileAlterationObserver(new File(path), pathname -> {
                for (String p : handlerPath.getExcludes()) {
                    if (pathname.getAbsolutePath().startsWith(p)) {
                        return false;
                    }
                }
                return true;
            });
            observer.addListener(this);
            monitor.addObserver(observer);
        }
    }
    
    @Override
    public void onDirectoryCreate(File directory) {
        this.fileScan.index(directory.getAbsolutePath());
    }
    
    @Override
    public void onFileCreate(File file) {
        Thing thing = FileConvertThing.convert(file);
        this.fileIndexDao.insert(thing);
    }
    
    @Override
    public void onFileDelete(File file) {
        Thing thing = FileConvertThing.convert(file);
        this.fileIndexDao.delete(thing);
    }
}
