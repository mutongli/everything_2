drop table if exists file_index;
create table if not exists file_index(
  name varchar(256) not null comment '文件名称',
  file_type varchar(64)   not null
  comment '文件类型：DOC, IMG, ARCHIVE, BIN, OTHER',
  path varchar(1024) not null comment '文件路径',
  depth int not null comment '文件目录层级深度'
);
create index name_index on file_index(name);